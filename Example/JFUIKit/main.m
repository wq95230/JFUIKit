//
//  main.m
//  JFUIKit
//
//  Created by wq95230 on 04/10/2022.
//  Copyright (c) 2022 wq95230. All rights reserved.
//

@import UIKit;
#import "JFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JFAppDelegate class]));
    }
}
