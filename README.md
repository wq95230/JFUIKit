# JFUIKit

[![CI Status](https://img.shields.io/travis/wq95230/JFUIKit.svg?style=flat)](https://travis-ci.org/wq95230/JFUIKit)
[![Version](https://img.shields.io/cocoapods/v/JFUIKit.svg?style=flat)](https://cocoapods.org/pods/JFUIKit)
[![License](https://img.shields.io/cocoapods/l/JFUIKit.svg?style=flat)](https://cocoapods.org/pods/JFUIKit)
[![Platform](https://img.shields.io/cocoapods/p/JFUIKit.svg?style=flat)](https://cocoapods.org/pods/JFUIKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JFUIKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JFUIKit'
```

## Author

wq95230, 952303557@qq.com

## License

JFUIKit is available under the MIT license. See the LICENSE file for more info.
